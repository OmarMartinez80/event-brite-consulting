FROM openjdk:8
VOLUME /tmp
ADD ./target/event-brite-consulting-0.0.1-SNAPSHOT.jar even-brite-consulting.jar
ENTRYPOINT ["java","-jar","/even-brite-consulting.jar"]